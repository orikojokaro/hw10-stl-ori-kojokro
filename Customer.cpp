#include "Customer.h"




Customer::Customer(std::string name) : _name(name)
{
}


Customer::~Customer()
{
}

double Customer::totalSum()
{
	double sum = 0;
	std::set<Item>::iterator iter;
	for (iter = _items.begin(); iter != _items.end(); iter++)
	{
		sum += (*iter).getPrice() *(*iter).getCount();
	}
	return sum;
}

void Customer::addItem(Item toAdd)
{
	std::set<Item>::iterator iter;
	if ((iter =_items.find(toAdd)) == _items.end())
	{
		_items.insert(toAdd);
	}
	else
	{
		Item replacment(toAdd);
		replacment.setCount(iter->getCount() + 1);
		_items.erase(toAdd);
		_items.insert(replacment);
	}
}
//remove item from the set
unsigned int Customer::removeItem(const Item& toRemove)
{
	Item theItem = *(_items.find(toRemove));
	if (_items.find(toRemove)->getCount() >= 2)
	{
		theItem.setCount(theItem.getCount() - 1);
		if (_items.erase(toRemove))
		{
			_items.insert(theItem);
			return 1;
		}
		else
		{
			return 0;
		}
	}
	else
	{
		return _items.erase(toRemove);
	}
}
