#pragma once
#include <iostream>
#include <string>
#define MIN_COUNT 1
class Item
{
	std::string _name;
	std::string _serialNumber;
	int _count; //default is 1, can never be less than 1!
	double _unitPrice; //always bigger than 0!
public://no need for copy ctor becuase there are no pointers.
	Item(std::string name, std::string serialNumber, double unitPrice);//ctor
	~Item();//dtor

	//getters and setters
	std::string getName()const { return _name; };
	void setName(std::string name) { _name = name; };

	std::string getSerialNumber()const { return _serialNumber; };
	void setSerialNumber(std::string serialNumber) { _serialNumber = serialNumber; };

	int getCount()const { return _count; };
	void setCount(int count);

	double getPrice()const { return _unitPrice;};
	void setPrice(double price);

	double totalPrice()const { return _count * _unitPrice; }; //returns _count*_unitPrice
	bool operator<(const Item& other)const; //compares the _serialNumber of those items.
	bool operator>(const Item& other)const; //compares the _serialNumber of those items.
	bool operator==(const Item& other)const; //compares the _serialNumber of those items.
};

