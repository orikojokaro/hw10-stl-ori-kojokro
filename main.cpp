#include<map>
#include <iostream>
#include"Customer.h"
#define ARR_SIZE 10
using namespace std;

enum {
	SIGN_UP = 1,
	UPDATE,
	TOP_BUYER,
	EXIT
};
enum {
	STOP,
	MILK,
	COOKIES,
	BREAD,
	CHOCOLATE,
	CHEESE,
	RICE,
	FISH,
	CHICKEN,
	CUCUMBER,
	TOMATO
};
enum {
	ADD_ITEMS = 1,
	REMOVE_ITEMS,
	BACK_TO_MENU
};
//this function prints the item list
//an arr of the items.
void printItemList(const Item * arr)
{
	for (unsigned int i = 0; i < ARR_SIZE; i++)
	{
		cout << i + 1 << ". " << "Price: " <<arr[i].getPrice() << " " << arr[i].getName() << endl;
	}
	cout << "what would you like?" << endl;
}
//returns false if the shopping cart is empty
bool printItemsOfCustomer(const Customer& cus)
{
	set<Item>::iterator iter;
	if (cus.getItems().size() != 0)
	{
		iter = cus.getItems().begin();
		for (unsigned int i = 0; iter != cus.getItems().end(); i++, iter++)
		{
			cout << i + 1 << "You have " << iter->getCount() << " " << iter->getName() << endl;
		}
		cout << "what would you like removed?" << endl;
		return true;
	}
	else
	{
		cout << "Your basket is empty" << endl;
		return false;
	}
}
//fixed cin if it enters a failed state.
void repairCin()
{
	std::cin.clear();
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}
//this function adds an item to the customer's set.
//input: the customer, the name of the item to add, 
void addItems(Customer& cus, Item * itemList)
{
	bool isFail = false;
	int input = 0;
	while (true)
	{
		cin >> input;//getting input
		while ((isFail = cin.fail()) || input < STOP || input > TOMATO)
		{
			if (isFail)
			{
				repairCin();
				isFail = false;
			}
			cout << "invalid input. Try again to add the item" << endl;
			cin >> input;
		}
		if (input != STOP)
		{
			cus.addItem(Item(itemList[input - 1]));
			cout << "you selected the item " << itemList[input - 1].getName() << endl;
		}
		else
		{
			break;
		}
	}
}
//this function finds and prints the highest paying customer. 
//input: all of the costumers.s
void printHighestPaying(map<string, Customer>& abcCustomers)
{
	map<string, Customer>::iterator iter = abcCustomers.begin();
	string name = iter->second.getName();
	double highestPay = iter->second.totalSum();
	double highestContender = 0;
	for (; iter != abcCustomers.end(); iter++)
	{
		if (highestPay < (highestContender = iter->second.totalSum()))
		{
			highestPay = highestContender;
			name = iter->second.getName();
		}
	}
	cout << name << " is the highest paying customer!" << endl << "He has paid " << highestPay << "dollars!" << endl;

}
//this function removes from the customer;s list
//input: the customer, the name of the product to remove and an array of all items possible.
void removeItems(Customer& cus, string name, const Item * itemList)
{
	int input = 0;
	bool isFail = false;
	int i = 0;
	std::set<Item>::iterator iter;
	while (true)
	{
		cin >> input;//getting input
		while ((isFail = cin.fail()) || input <= STOP || input > (int)cus.getItems().size())//must check the size each iteration because it might change due to deletes
		{
			if (input == STOP)
			{
				break;
			}
			if (isFail)
			{
				repairCin();
			}
			cout << "invalid input. Try again" << endl;
			cin >> input;
			
		}
		iter = cus.getItems().begin();
		for (i = 1; i < input; i++)
		{
			iter++;
		}
		if (input != STOP &&cus.removeItem(*iter) != 0)
		{
			cout << "removed successfuly" << endl;
			printItemsOfCustomer(cus);
		}
		else if (input != STOP)
		{
			cout << "couldn't remove the item" << endl;
		}
		else
		{
			break;
		}
	}
}
int main()
{
	int input= 0;
	bool isFail = false;
	string name = "";
	map<string, Customer> abcCustomers;
	Item itemList[ARR_SIZE] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00008", 31.65),
		Item("chicken","00007",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32) };
	while (input != EXIT)
	{
		cout << "Welcome to MagshiMart!" <<endl<<
			"1.      to sign as customer and buy items" <<endl <<
			"2.      to uptade existing customer's items" << endl<<
			"3.      to print the customer who pays the most" << endl<<
			"4.      to exit" << endl;
		cin >> input;
		while ((isFail = cin.fail())|| input < SIGN_UP || input > EXIT )
		{
			if (isFail)
			{
				repairCin();
			}
			cout << "invalid input. Try again" << endl;
			cin >> input;
		}
		switch (input)
		{
		case SIGN_UP:
			cout << "Enter your name:" << endl;
			cin >> name;//getting the name
			if (abcCustomers.find(name) != abcCustomers.end())
			{
				cout << "The name already exists" << endl;
				break;
			}
			else
			{
				abcCustomers.emplace(name, Customer(name));
				printItemList(itemList);//printing the itmes he can buy
				addItems(abcCustomers.find(name)->second, itemList);
			}
			break;
		case UPDATE:
			cout << "Enter your name:" << endl;
			cin >> name;//getting the name
			if (abcCustomers.find(name) == abcCustomers.end())
			{
				cout << "The name doesn't exist" << endl;
				break;
			}
			else
			{
				while (input != BACK_TO_MENU)
				{
					cout << "1.      Add items" << endl <<
						"2.      Remove items" << endl <<
						"3.      Back to menu" << endl;

					cin >> input;//getting input
					while ((isFail = cin.fail()) || input < ADD_ITEMS || input > BACK_TO_MENU)
					{
						if (isFail)
						{
							repairCin();
						}
						cout << "invalid input. Try again" << endl;
						cin >> input;
					}
					switch (input)
					{
					case ADD_ITEMS:
						printItemList(itemList);//printing the itmes he can buy				
						addItems(abcCustomers.find(name)->second, itemList);
						break;
					case REMOVE_ITEMS:
						if (printItemsOfCustomer(abcCustomers.find(name)->second))//printing the itmes he can buy
						{
							removeItems(abcCustomers.find(name)->second, name, itemList);
						}
						break;
					case BACK_TO_MENU:
						break;
					}
				}
			}
			break;
		case TOP_BUYER:
		{
			printHighestPaying(abcCustomers);
			break;
		}
		case EXIT: 
		{
			break;
		}
		}
	}
	return 0;
}