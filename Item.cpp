#include "Item.h"



Item::Item(std::string name, std::string serialNumber, double unitPrice): _name(name), _serialNumber(serialNumber), _unitPrice(unitPrice), _count(MIN_COUNT)
{
}


Item::~Item()
{
}

void Item::setCount(int count)
{
	if (count < MIN_COUNT)
	{
		throw (std::string("count must be >= 1"));
	}
	else
	{
		_count = count;
	}
}

void Item::setPrice(double price) 
{
	if (price > 0)
	{
		_unitPrice = price;
	}
	else
	{
		throw(std::string("unit price must be above 0"));
	}
}

bool Item::operator<(const Item & other)const
{
	if (_serialNumber < other.getSerialNumber())
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Item::operator>(const Item & other) const
{
	if (*this < other)
	{
		return false;
	}
	else
	{
		return true;
	}
}

bool Item::operator==(const Item & other)const
{
	if (_serialNumber == other.getSerialNumber())
	{
		return true;
	}
	else
	{
		return false;
	}
}
