#pragma once
#include <iostream>
#include <string>
#include <set>
#include "Item.h"
class Customer
{
private:
	std::string _name;
	std::set<Item> _items;
public://no need for copy ctor becuase there are no pointers.
	Customer(std::string name);//ctor
	~Customer();//dtor
	//getters and setters
	std::string getName() const { return _name; };
	void setName(std::string name) { _name = name; };
	const std::set<Item>& getItems()const { return _items; };
	//setter for _items would give the user too much ability to f*** the class up.
	double totalSum(); //returns the total sum for payment
	void addItem(Item toAdd); //add item to the set
	unsigned int removeItem(const Item &toRemove); //remove item from the set
};

